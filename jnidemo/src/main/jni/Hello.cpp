//
// Created by Shuiyouwen on 2018/4/12.
//
#include "cn_com_jnidemo_JniUtils.h"

JNIEXPORT jstring JNICALL Java_cn_com_jnidemo_JniUtils_getJniString
  (JNIEnv *, jclass){
    // new 一个字符串，返回Hello World
    return env -> NewStringUTF("Hello World");
}
