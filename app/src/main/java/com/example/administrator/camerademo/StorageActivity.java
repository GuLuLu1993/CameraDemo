package com.example.administrator.camerademo;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.File;

/**
 * 持久化的相关操作
 * Created by Shui on 2018/4/11.
 */

public class StorageActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage);
        File filesDir = getFilesDir();
        Log.d("StorageActivity", "getFilesDir" + filesDir.getAbsolutePath());
        Log.d("StorageActivity", "getCacheDir" + getCacheDir().getAbsolutePath());
        File externalFilesDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (externalFilesDir != null) {
            Log.d("StorageActivity", "externalFilesDir" + externalFilesDir.getAbsolutePath());
        }
    }
}
