package com.example.administrator.camerademo.customercamera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraDevice.StateCallback;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Size;
import android.view.TextureView;

import com.example.administrator.camerademo.R;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Shui on 2018/4/12.
 */

@SuppressLint("NewApi")
public class CustomerCameraActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener, StateCallback {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_camera);
        TextureView textureView = findViewById(R.id.textureView);
        textureView.setSurfaceTextureListener(this);
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        //准备就绪
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String cameraId = setupCamera();
            openCamera(cameraId);
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {
        //缓冲大小变化
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        //销毁
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        //更新
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("MissingPermission")
    private void openCamera(String cameraId) {
        CameraManager cameraManager = (CameraManager) getSystemService(CAMERA_SERVICE);
        if (cameraManager == null) {
            return;
        }
        try {
            cameraManager.openCamera(cameraId, this, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private String setupCamera() {
        try {
            CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            if (cameraManager == null) {
                return "";
            }
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                Integer lensFacing = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
                if (lensFacing != null && lensFacing == CameraCharacteristics.LENS_FACING_FRONT) {
                    continue;
                }
                //获取摄像头支持的所有输出格式和尺寸
                StreamConfigurationMap configurationMap = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (configurationMap != null) {
                    Size[] outputSizes = configurationMap.getOutputSizes(ImageFormat.JPEG);
                    Log.d("CustomerCameraActivity", "outputSizes:" + Arrays.toString(outputSizes));
                    Size maxSize = Collections.max(Arrays.asList(outputSizes), new Comparator<Size>() {
                        @Override
                        public int compare(Size size1, Size size2) {
                            //signum返回正负数 1正 0 0 -1负
                            return Long.signum(size1.getWidth() * size1.getHeight() - size2.getWidth() * size2.getHeight());
                        }
                    });
                    Log.d("CustomerCameraActivity", "maxSize:" + maxSize);
                }
                return cameraId;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onOpened(@NonNull CameraDevice cameraDevice) {
        //拍照打开

    }

    @Override
    public void onDisconnected(@NonNull CameraDevice cameraDevice) {

    }

    @Override
    public void onError(@NonNull CameraDevice cameraDevice, int i) {

    }
}
